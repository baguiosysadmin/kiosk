let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css').sourceMaps()
   .styles([
	  'public/css/bootstrap.css', 
	  'public/css/style.css',
	  'public/css/mdb.css',
     'public/css/tooplate-style.css',
     'public/css/font-awesome.min.css',
     'public/Datatables/datatables.min.css',
	], 'public/css/app.css')
   .js([
   		'public/js/mdb.min.js',
   		'public/js/popper.min.js',
      'public/js/style.js',
      'public/Datatables/datatables.min.js'
   	],'public/js/app.js')
