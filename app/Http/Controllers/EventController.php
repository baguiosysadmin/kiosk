<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index() {
    	return view("layouts.events");
    }

    public function create() {
    	return view("layouts.event-create");
    }

    public function edit($id) {
    	return view("layouts.event-edit", compact('id'));
    }
}
