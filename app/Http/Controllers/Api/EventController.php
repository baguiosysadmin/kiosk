<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;

class EventController extends Controller
{
    public function index() 
    {
        /*$events = Event::all();
        return response()->json($events);*/

        $events = Event::all();
        return datatables($events)->make(true);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
    
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {
    	
    }
}
