<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Department;

class DepartmentController extends Controller
{
    public function index() 
    {

    	/*$departments = Department::with('official')->get();

    	return response()->json($departments);*/

        $departments = Department::with('official')->get();
        return datatables($departments)->make(true);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
        $department = Department::find($id);

        return response()->json($department);
    }

    public function update($id, Request $request)
    {
        $department = Department::find($id);

        $department->room = $request->get('room');
        $department->name = $request->get('name');
        $department->location = $request->get('location');
        $department->contact_number = $request->get('contact_number');
        $department->official_id = $request->get('official_id');

        $department->update();
        return response()->json($department);
    }

    public function destroy($id)
    {
    	
    }
}
