<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\File;

class FileController extends Controller
{
    public function index() 
    {

        //
    }

    public function store(Request $request) 
    {
    	$request->validate([
            'file' => 'required|file|max:10000024'
        ]);

        $file = "fileName".time().'.'.request()->file->getClientOriginalExtension();

        $upload = new File();

        $upload->filename = request()->file->getClientOriginalName();
        $upload->mime_type =  request()->file->getMimeType();
        $upload->path = $file;
        $upload->charter_id = request()->charter_id;

        $upload->save();

        $request->file->storeAs('uploads',$file);

        return response()->json($upload);
    }

    public function show($id)
    {
    	//
    }

    public function update($id, Request $request)
    {
    	//
    }

    public function destroy($id)
    {
    	//
    }
}
