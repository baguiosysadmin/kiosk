<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Charter;

class CharterController extends Controller
{
    public function index() 
    {
        $charters = Charter::with('files')->get();
        return response()->json($charters);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
    
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {
    	
    }
}
