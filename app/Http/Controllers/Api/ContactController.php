<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index() 
    {

    	// $users = User::orderBy("id", "DESC")->paginate(5);

     //    return response()->json($users);
    }

    public function store(Request $request) 
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email',
    		'subject' => 'required',
            'message' => 'required'
    	]);
    	
    	$contact = new Contact();

    	$contact->name = $request->get('name');
    	$contact->email = $request->get('email');
    	$contact->subject = $request->get('subject');
        $contact->message = $request->get('message');
    	$contact->save();
    	return response()->json($contact);

    }

    public function show($id)
    {
    	/*$user = User::find($id);

    	return response()->json($user);*/
    }

    public function update($id, Request $request)
    {
    	/*$user = User::find($id);

    	$user->name = $request->get('name');
    	$user->email = $request->get('email');

    	$user->update();
    	return response()->json($user);*/
    }

    public function destroy($id)
    {
    	/*$user = User::find($id);

    	$user->delete();

    	return response()->json('User Deleted');*/
    }
}
