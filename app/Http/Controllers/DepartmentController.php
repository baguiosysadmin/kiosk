<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index() {
        return view("layouts.departments");
    }

    public function create() {
        return view("layouts.department-create");
    }

    public function edit($id) {
        return view("layouts.department-edit", compact('id'));
    }
}

