<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>KIOSK || City Government of Baguio</title>
    <!-- load CS -->
                    
</head>
<body>
    <div id="tm-bg"></div>
        <div id="tm-wrap">
            <div class="tm-main-content" id="app">
                <div class="container tm-site-header-container">
                <div class="row">
                    
                        <div class="container">
                            <div class="grid">
                                <div class="grid__item" id="home-link">                                           
                                    @include("modals.citizen-charter")                                  
                                </div>
                                <div class="grid__item" id="team-link">
                                   @include("modals.directory")
                                </div>
                                 <div class="grid__item" id="team-link">
                                    @include("modals.event")
                                </div>
                                <div class="grid__item">
                                     @include("modals.bulletin-board")
                                </div>
                                <div class="grid__item">
                                    @include("modals.contact")
                                </div>
                            </div> 
                        </div>                       
                </div>                
            </div>
            <footer>
                <p class="small tm-copyright-text">Copyright &copy; <span class="tm-current-year">2018</span> || City Government of Baguio || <a rel="nofollow" href="http://baguio.gov.ph" class="tm-text-highlight">Powered by MITD</a></p>
            </footer>
        </div> 
    </div>
    <!-- load JS -->                                   
</body>
</html>