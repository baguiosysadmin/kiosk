<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>KIOSK || City Government of Baguio</title>
    <!-- load CS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">                  
</head>
<body>
    <div id="tm-bg"></div>
        <div id="tm-wrap">
            <div class="tm-main-content-2" id="app">
                @yield("content")           
            </div>
        </div> 
    </div>
    <!-- load JS -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>