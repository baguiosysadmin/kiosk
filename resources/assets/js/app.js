
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.moment = require('moment');
window.$ = require('jquery');
window.JQuery = require('jquery');

import swal from 'sweetalert2'
window.swal = swal;


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('index', require('./components/Index.vue'));
Vue.component('users', require('./components/User.vue'));
Vue.component('user-create', require('./components/UserCreate.vue'));
Vue.component('user-edit', require('./components/UserEdit.vue'));
Vue.component('citizen-charter', require('./components/Modals/CitizenCharterModal.vue'));
Vue.component('directory', require('./components/Modals/DirectoryModal.vue'));
Vue.component('event', require('./components/Modals/EventModal.vue'));
Vue.component('bulletin-board', require('./components/Modals/BulletinModal.vue'));
Vue.component('contact', require('./components/Modals/ContactModal.vue'));
Vue.component('departments', require('./components/Directory.vue'));
Vue.component('department-create', require('./components/DirectoryCreate.vue'));
Vue.component('department-edit', require('./components/DirectoryEdit.vue'));
Vue.component('events', require('./components/Event.vue'));
const app = new Vue({
    el: '#app'
});
