<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('departments')->insert(array(
		     array(
		       	'name' => 'Accounting Office',
		       	'room' => '101',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-6522 or (074)300-2212',
		       	'official_id' => 2,
		       	'description' => ''
		     ),
		     array(
		       	'name' => 'Permit and Licensing Division',
		       	'room' => '102',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 442-8920',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Commission on Audit (Office of the supervising Auditor)',
		       	'room' => '103',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Commission on Audit (Office of the City Auditor)',
		       	'room' => '104',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Teller',
		       	'room' => '105',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-5410 or (074) 442-2789',
		       	'official_id' => 17,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Cashier',
		       	'room' => '106',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-5410 or (074) 442-2789',
		       	'official_id' => 17,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Treasury Office (ADMIN/RPTD)',
		       	'room' => '107',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-5410 or (074) 442-2789',
		       	'official_id' => 17,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Treasury Office (CID/BTFD)',
		       	'room' => '108',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-5410 or (074) 442-2789',
		       	'official_id' => 17,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Business One-Stop Shop (CEDULA)',
		       	'room' => '109',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '',
		       	'official_id' => 17,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Hall Clinic',
		       	'room' => '110',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '',
		       	'official_id' => 37,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Baguio City Gpvernment Employees Multi-purpose Cooperative',
		       	'room' => '111',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '',
		       	'official_id' => 37,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Session Hall',
		       	'room' => '112',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '',
		       	'official_id' => 37,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Sangunian Administrative Division Office',
		       	'room' => '113',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-6505 or (074) 444-2343',
		       	'official_id' => 15,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'SP Comittee Secretarial Services Division',
		       	'room' => '114',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-6505 or (074) 444-2343',
		       	'official_id' => 15,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Budget Office',
		       	'room' => '115',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-6529 or (074) 300-2916',
		       	'official_id' => 5,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Planning and Development and Offices',
		       	'room' => '116',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-6549 or (074) 442-6607',
		       	'official_id' => 14,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'CDRRMC',
		       	'room' => '117',
		       	'location' => 'Baguio City Hall (Ground Floor)',
		       	'contact_number' => '(074) 300-2410 or (074) 300-6524',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Mayors Office',
		       	'room' => '201',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-8920',
		       	'official_id' => 21,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Mayors Office (Receiving Area)',
		       	'room' => '202',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6503',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Elmer O. Datuin (City Councilor)',
		       	'room' => '203',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-8919',
		       	'official_id' => 27,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Francisco Roberto A. Ortega VI (City Councilor)',
		       	'room' => '204',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 446-9101',
		       	'official_id' => 23,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Lilia A. Fariñas (City Councilor)',
		       	'room' => '205',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-5492',
		       	'official_id' => 24,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Maria Maylen Victoria G. Yaranon (City Councilor)',
		       	'room' => '206',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-8915',
		       	'official_id' => 29,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'MITD Technical Room',
		       	'room' => '207' ,
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Assesors Office',
		       	'room' => '208',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6526',
		       	'official_id' => 4,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'MITD Data Center',
		       	'room' => '209',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Department Of Interior and Local Government',
		       	'room' => '210',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'SP Research Division',
		       	'room' => '211',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6505 or (074) 444-2343',
		       	'official_id' => 15,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Steno-Clerical Service Division',
		       	'room' => '212',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6505 or (074) 444-2343',
		       	'official_id' => 15,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Leandro B. Yangot Jr. (City Councilor)',
		       	'room' => '213',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 446-9102',
		       	'official_id' => 31,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Faustino A. Olowan (City Councilor)',
		       	'room' => '214',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-8913',
		       	'official_id' => 28,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Benny O. Bomogao (City Councilor)',
		       	'room' => '215',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '',
		       	'official_id' => 30,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Peter C. Fianza (City Councilor)',
		       	'room' => '216',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-8914',
		       	'official_id' => 32,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Arthur Allad-iw (City Councilor)',
		       	'room' => '217',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-5496',
		       	'official_id' => 25,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Elaine D. Sembrano (City Councilor)',
		       	'room' => '218',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-5490',
		       	'official_id' => 33,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Congressmans Office',
		       	'room' => '219',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '',
		       	'official_id' => 19,
		       	'description' => ''
		    ),array(
		       	'name' => 'Human Resource Management Office',
		       	'room' => '220',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6542 or (074) 300-4110',
		       	'official_id' => 12,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Joel A. Alansab (City Councilor)',
		       	'room' => '221',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-8908',
		       	'official_id' => 36,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Hon. Edgar M. Avilla (City Councilor)',
		       	'room' => '222',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-5493',
		       	'official_id' => 26,
		       	'description' => ''
		    ),array(
		       	'name' => 'Public Employment Service Office (PESO)',
		       	'room' => '223',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6503',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Vice Mayor Office',
		       	'room' => '224',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 442-5495',
		       	'official_id' => 22,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Legal Office',
		       	'room' => '225',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-6544 or (074) 300-4410',
		       	'official_id' => 13,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Office for Administration',
		       	'room' => '226',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-2410 or (074) 300-6524',
		       	'official_id' => 3,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Public Information Office',
		       	'room' => '227',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '(074) 300-2410 or (074) 300-6524',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Canteen',
		       	'room' => '228',
		       	'location' => 'Baguio City Hall (Second Floor)',
		       	'contact_number' => '',
		       	'official_id' => 37,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'General Service Office',
		       	'room' => 'B01',
		       	'location' => 'Baguio City Hall (Basement)',
		       	'contact_number' => '(074) 300-6537 or (074) 442-8924',
		       	'official_id' => 10,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Mayors Office Special Services Division',
		       	'room' => 'B02',
		       	'location' => 'Baguio City Hall (Basement)',
		       	'contact_number' => '(074) 300-5012 or (074) 300-5013',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Peoples Law Inforcement Board (PLEB)',
		       	'room' => 'B03',
		       	'location' => 'Baguio City Hall (Basement)',
		       	'contact_number' => '(074) 300-5016 or 442-8932 ',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Liga ng mga Barangay Office',
		       	'room' => 'B04',
		       	'location' => 'Baguio City Hall (Basement)',
		       	'contact_number' => '',
		       	'official_id' => 20,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Registry of Deeds',
		       	'room' => 'B05',
		       	'location' => 'Baguio City Hall (Basement)',
		       	'contact_number' => '',
		       	'official_id' => 1,
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Environment and Parks Management Office',
		       	'room' => 'B06',
		       	'location' => 'Baguio City Hall (Basement)',
		       	'contact_number' => '(074) 300-6535 or (074) 442-2642',
		       	'official_id' => 9,
		       	'description' => ''
		    )
		));
    }
}
