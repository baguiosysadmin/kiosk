<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('files')->insert(array(
//new
		    array(
		       	'filename' => 'A_Food-related Businesses(New).pdf',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'A_FoodRelated(New)',
		       	'description' => 'Restaurants, Canteens, Sari-sari Stores, Video & Disco Bars, Eatery, Food Carts, Refreshments and Snacks, Vegetable/Fruit Retailers, and other Food-related establishments (lodging houses, apartelles, hotels/motels, coffee shops, consumers cooperative)',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'A1_Food-related Businesses(New).pdf',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'A1_FoodRelated(New)',
		       	'description' => 'Disco Bars, Cabarets, Gay bars',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B_Non-food Related Businesses(New)',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'B_NonFoodRelated(New)',
		       	'description' => 'Shuttle Vans, Pharmacy/Drug Stores, Manpower/Recruitment Agencies, Dance Studios, Review Centers, Tutorial Schools, Gym/Fitness Instructors, Caddy Boys, Pony Boys, Janitorial Services',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B1_Non-food Related Businesses(New)',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'B1_NonFoodRelated(New)',
		       	'description' => 'Spa, Massage Clinics, Massage Therapy',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B2_Non-food Related Businesses(New)',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'B2_NonFoodRelated(New)',
		       	'description' => 'Septage Haulers',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B3_Non-food Related Businesses(New)',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'B3_NonFoodRelated(New)',
		       	'description' => 'Barber shops, Beauty Parlors, Security Agencies',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B4_Non-food Related Businesses(New)',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'B4_NonFoodRelated(New)',
		       	'description' => 'Dental Clinic, Veterinary Clinic, Medical and Diagnostic                      Laboratory',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'C_Other Businesses(New)',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'C_Other(New)',
		       	'description' => 'Not requiring Health Certificates (Real Estate, Dry Goods, Footwear, Ukay-ukay, Kitchenwares, Banks, Cellphone Store, Hardware, etc.)',
		       	'charter_id' => 1
		    ),

//renew
		    array(
		       	'filename' => 'A_Food-related Businesses(Renew).pdf',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'A_FoodRelated(New)',
		       	'description' => 'Restaurants, Canteens, Sari-sari Stores, Video & Disco Bars, Eatery, Food Carts, Refreshments and Snacks, Vegetable/Fruit Retailers, and other Food-related establishments (lodging houses, apartelles, hotels/motels, coffee shops, consumers cooperative)',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'A1_Food-related Businesses(Renew).pdf',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'A1_FoodRelated(Renew)',
		       	'description' => 'Disco Bars, Cabarets, Gay bars',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B_Non-food Related Businesses(Renew)',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'B_NonFoodRelated(Renew)',
		       	'description' => 'Shuttle Vans, Pharmacy/Drug Stores, Manpower/Recruitment Agencies, Dance Studios, Review Centers, Tutorial Schools, Gym/Fitness Instructors, Caddy Boys, Pony Boys, Janitorial Services',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B1_Non-food Related Businesses(Renew)',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'B1_NonFoodRelated(Renew)',
		       	'description' => 'Spa, Massage Clinics, Massage Therapy',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B2_Non-food Related Businesses(Renew)',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'B2_NonFoodRelated(Renew)',
		       	'description' => 'Septage Haulers',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B3_Non-food Related Businesses(Renew)',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'B3_NonFoodRelated(Renew)',
		       	'description' => 'Barber shops, Beauty Parlors, Security Agencies',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'B4_Non-food Related Businesses(Renew)',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'B4_NonFoodRelated(Renew)',
		       	'description' => 'Dental Clinic, Veterinary Clinic, Medical and Diagnostic                      Laboratory',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'C_Other Businesses(Renew)',
		       	'type' => 2,
		       	'mime_type' => '',
		       	'path' => 'C_Other(Renew)',
		       	'description' => 'Not requiring Health Certificates (Real Estate, Dry Goods, Footwear, Ukay-ukay, Kitchenwares, Banks, Cellphone Store, Hardware, etc.)',
		       	'charter_id' => 1
		    ),
		    array(
		       	'filename' => 'D_Water Businesses',
		       	'type' => 3,
		       	'mime_type' => '',
		       	'path' => 'D_Water',
		       	'description' => 'Water Refilling Stations, Water Delivery Services and other Water-related Businesses',
		       	'charter_id' => 1
		    ),
//new

		    array(
		       	'filename' => 'Health Certificate',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'health_certificate',
		       	'description' => 'Issuance of Health Certificate; Health Certificate with Medical Certificate/Physical Examination/     Fit to Work',
		       	'charter_id' => 2
		    ),
		    array(
		       	'filename' => 'Working permit',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'working_permit',
		       	'description' => 'Public Service Employment Office (PESO): Processing of Working Permits',
		       	'charter_id' => 3
		    ),
		    array(
		       	'filename' => 'Townsite Sales Application',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'townsite_sales_application',
		       	'description' => 'Processing of Townsite Sales Application (TSA), Residential Free Patent Application (RFPA) and Miscellaneous Sales Application (MSA)',
		       	'charter_id' => 4
		    ),
		    array(
		       	'filename' => 'Medical Assistance',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'medical_assistance',
		       	'description' => 'Issuance of Social Case Study Report/Referral Letter (Medical Assistance)',
		       	'charter_id' => 5
		    ),
		    array(
		       	'filename' => 'Food Assistance',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'food_assistance',
		       	'description' => 'Food Assistance',
		       	'charter_id' => 5
		    ),
		    array(
		       	'filename' => 'Provision of Assistance to Individual in Crisis Situation',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'aics',
		       	'description' => 'Provision of Assistance to Individual in Crisis  Situation (AICS)',
		       	'charter_id' => 5
		    ),
		    array(
		       	'filename' => 'Capital Assistance',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'capital_assistance',
		       	'description' => 'Provision of Capital Assistance (Self-Employment Assistance Program)',
		       	'charter_id' => 5
		    ),
		    array(
		       	'filename' => 'New Business Permit',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'new_bussinness_permit',
		       	'description' => 'Application for New Business Permit',
		       	'charter_id' => 6
		    ),
		    array(
		       	'filename' => 'Special Business Permit',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'special_bussinness_permit',
		       	'description' => 'Application for Special Business Permit',
		       	'charter_id' => 6
		    ),
		    array(
		       	'filename' => 'Tax Declaration',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'tax_declaration',
		       	'description' => 'To be updated',
		       	'charter_id' => 7
		    ),
		    array(
		       	'filename' => 'Assessment of Land',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'assesment_land',
		       	'description' => 'Assesment of Land',
		       	'charter_id' => 8
		    ),
		    array(
		       	'filename' => 'Assessment of Machinery',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'assesment_machinary',
		       	'description' => 'Assessment of Machinery',
		       	'charter_id' => 8
		    ),
		    array(
		       	'filename' => 'Building Permit',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'business_permit',
		       	'description' => 'Building Permit',
		       	'charter_id' => 9
		    ),
		    array(
		       	'filename' => 'Certificate of Occupancy',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'certificate_occupancy',
		       	'description' => 'Certificate of Occupancy',
		       	'charter_id' => 10
		    ),
		    array(
		       	'filename' => 'Registry Documents',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'registry_documents',
		       	'description' => 'Registration of Civil Registry Documents (Birth, Marriage and Death Certificates)',
		       	'charter_id' => 11
		    ),
		    array(
		       	'filename' => 'Registry Documents Issuance',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'registry_documents_issuance',
		       	'description' => 'Issuance of Civil Registry Documents (Birth, Marriage, Death Certificates)',
		       	'charter_id' => 11
		    ),
		    array(
		       	'filename' => 'Registry Documents Issuance',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'registry_documents_issuance',
		       	'description' => 'Issuance of Civil Registry Documents (Birth, Marriage, Death Certificates)',
		       	'charter_id' => 11
		    ),
		    array(
		       	'filename' => 'To be updated',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'Registration of Legal Documents',
		       	'description' => 'To be updated',
		       	'charter_id' => 12
		    ),
		     array(
		       	'filename' => 'QUISI-JUDICIAL SERVICES: Petition for Correction of Clerical Error (CCE) / Change of First Name (CFN) Under RA 9048/ RA 10172',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'merriage_license',
		       	'description' => 'Application for Marriage License',
		       	'charter_id' => 13
		    ),
		    array(
		       	'filename' => 'Marriage License',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'merriage_license',
		       	'description' => 'Application for Marriage License',
		       	'charter_id' => 14
		    ),
		    array(
		       	'filename' => 'Parks and Recreation Permit',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'parks_recreation',
		       	'description' => 'Issuance of Parks and Recreation Permit',
		       	'charter_id' => 15
		    ),
		    array(
		       	'filename' => 'Sewer Compliance Certificate',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'sewer_compliance',
		       	'description' => 'Issuance of Parks and Recreation Permit',
		       	'charter_id' => 16
		    ),
		    array(
		       	'filename' => 'Tree Cutting Permit',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'tree_cutting',
		       	'description' => 'Issuance of Tree Cutting Permit',
		       	'charter_id' => 17
		    ),
		    array(
		       	'filename' => 'PLEB',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'pleb',
		       	'description' => 'People’s Law Enforcement Board (PLEB): Issuance of PLEB Clearance',
		       	'charter_id' => 18
		    ),
		    array(
		       	'filename' => 'POSD',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'posd',
		       	'description' => 'Public Order and Safety Division (POSD): Issuance of Mayor’s Clearance for Local Employment, Travel Abroad, Marriage Contract with military personnel; Security Guard; PNP recruit, etc.)',
		       	'charter_id' => 18
		    ),
		    array(
		       	'filename' => 'SSD',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'ssd',
		       	'description' => 'Special Services Division (SSD): Issuance of Barangay Business Clearance (within City Market and Punong Barangay)',
		       	'charter_id' => 18
		    ),
		    array(
		       	'filename' => 'AICS',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'aics',
		       	'description' => 'Provision of Assistance to Individual in Crisis Situation (AICS)',
		       	'charter_id' => 19
		    ),
		    array(
		       	'filename' => 'Pre-Marriage Counseling',
		       	'type' => 1,
		       	'mime_type' => '',
		       	'path' => 'premerriage_counseling',
		       	'description' => 'Pre-Marriage Counseling (PMC) Service',
		       	'charter_id' => 20
		    ),
		));
    }
}
