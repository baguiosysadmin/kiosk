<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('events')->insert(array(
        	array(
		    	'title' => 'Rizal Day',
		       	'details' => 'Celebration for our national hero',
		       	'event_date' => now()
		    ),
		   	array(
		    	'title' => 'Independence Day',
		       	'details' => 'Celebration for our independence day',
		       	'event_date' => now()
		    )
		));
    }
}
