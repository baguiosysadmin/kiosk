<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
	        DepartmentsTableSeeder::class
	    ]);

	    $this->call([
	        OfficialsTableSeeder::class
	    ]);

        $this->call([
            ChartersTableSeeder::class
        ]);

        $this->call([
            FilesTableSeeder::class
        ]);
        $this->call([
            EventsTableSeeder::class
        ]);
    }
}
