<?php

use Illuminate\Database\Seeder;

class OfficialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('officials')->insert(array(
        	array(
		    	'prefix' => '',
		       	'name' => 'To be updated',
		       	'description' => ''
		    ),
		   	array(
		    	'prefix' => 'MR.',
		       	'name' => 'ANTONIO L. TABIN',
		       	'description' => 'City Accountant'
		    ),
		    array(
		       	'prefix' => 'ATTY.',
		       	'name' => 'CARLOS M. CANILAO',
		       	'description' => 'City Administrator'
		    ),
		    array(
		       	'prefix' => 'MS.',
		       	'name' => 'MARIA ALMAYA C. ADDAWE',
		       	'description' => 'City Assessor'
		    ),
		    array(
		       	'prefix' => 'ATTY.',
		       	'name' => 'LETICIA O. CLEMENTE',
		       	'description' => 'City Budget Officer' 
		    ),
		    array(
		       	'prefix' => 'ENGR.',
		       	'name' => 'NAZITA F. BAÑEZ',
		       	'description' => 'City Building Official'
		    ),
		    array(
		       	'prefix' => 'MS.',
		       	'name' => 'SYLVIA R. LAUDENCIA',
		       	'description' => 'City Civil Registrar'
		    ),
		    array(
		       	'prefix' => 'ENGR.',
		       	'name' => 'EDGAR VICTORIO R. OLPINDO',
		       	'description' => 'City Engineer'
		    ),
		    array(
		       	'prefix' => 'MS.',
		       	'name' => 'CORDELIA C. LACSAMANA',
		       	'description' => 'City Environment and Parks Management Officer'
		    ),
		    array(
		       	'prefix' => 'MR.',
		       	'name' => 'EUGENE D. BUYUCAN',
		       	'description' => 'City General Services Officer'
		    ),
		    array(
		       	'prefix' => 'DR.',
		       	'name' => 'ROWENA P. GALPO',
		       	'description' => 'City Health Officer'
		    ),
		    array(
		       	'prefix' => 'ATTY.',
		       	'name' => 'AUGUSTIN P. LABAN III',
		       	'description' => 'City Human Resource Management Officer'
		    ),
		    array(
		       	'prefix' => 'ATTY.',
		       	'name' => 'MELCHOR CARLOS R. RABANES',
		       	'description' => 'Assistant City Legal Officer and OIC'
		    ),
		    array(
		       	'prefix' => 'ENGR.',
		       	'name' => 'EVELYN B. CAYAT',
		       	'description' => 'Planning and Development Office'
		    ),array(
		       	'prefix' => 'ATTY.',
		       	'name' => 'BRENNER L. BENGWAYAN',
		       	'description' => 'City Secretary'
		    ),
		    array(
		       	'prefix' => 'MS.',
		       	'name' => 'BETTY F. FANGASAN',
		       	'description' => 'City Administrator'
		    ),
		    array(
		       	'prefix' => 'MR.',
		       	'name' => 'ALEX B. CABARRUBIAS',
		       	'description' => 'City Treasurer'
		    ),
		    array(
		       	'prefix' => 'DR.',
		       	'name' => 'BRIGIT P. PIOK',
		       	'description' => 'City Veterinary'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'MARQUEZ O. GO',
		       	'description' => 'Representative, Lone District of Baguio'
		    ),
		    array(
		       	'prefix' => 'ATTY.',
		       	'name' => 'GEORGE A. FORTEA',
		       	'description' => ''
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'MAURICIO G. DOMOGAN',
		       	'description' => 'City Mayor'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'EDISON R. BILOG',
		       	'description' => 'City Vice Mayor'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'FRANCISCO ROBERTO A. ORTEGA',
		       	'description' => 'Chair, Public Protection and Safety, Peace and Order'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'LILIA A. FARIÑAS',
		       	'description' => 'Chair, Social Services, Women and Urban Poor'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'ARTHUR L. ALLAD-IW',
		       	'description' => 'Chair, Employment, Livelihood, Cooperative and Handicapped and Differently-Abled Persons'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'EDGAR M. AVILA',
		       	'description' => 'Chair, Urban Planning, Lands and Housing and Division I, Barangay Administrative Cases'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'Hon. Elmer O. Datuin',
		       	'description' => 'Chair, Appropriate & Finance, Tourism, Special Events, Parks and Playgrounds'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'FAUSTINO A. OLOWAN',
		       	'description' => 'Chair, Laws, Human Rights and Justice'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'MARIA MYLEN VICTORIA G. YARANON',
		       	'description' => 'Chair, Public Works'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'BENNY O. BOMOGAO',
		       	'description' => 'Chair, Public Utilities, Transportation and Traffic Legislation'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'LEANDRO B. YANGOT JR.',
		       	'description' => 'Chair, Market, Trade, Commerce and Agriculture'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'PETER C. FIANZA',
		       	'description' => 'Chair, Education, Culture, and Historical Research and Division II, Barangay Administrative Cases'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'ELAINE D. SEMBRANO',
		       	'description' => 'Chair, Health and Sanitation, Ecology and Environment Protection'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'EDISON R. BILOG',
		       	'description' => 'City Vice Mayor'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'MICHAEL L. LAWANA',
		       	'description' => 'ABC President and Chair, Barangay Affairs and Youth Welfare and Sports Development'
		    ),
		    array(
		       	'prefix' => 'HON.',
		       	'name' => 'JOEL A. ALANGSAB',
		       	'description' => 'Chair, Ethics, Governmental Affairs and Personnel'
		    ),
		    array(
		    	'prefix' => '',
		       	'name' => 'N/A',
		       	'description' => ''
		    )
		));
    }
}
