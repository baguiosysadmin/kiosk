<?php

use Illuminate\Database\Seeder;

class ChartersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charters')->insert(array(
		    array(
		       	'name' => 'Issuance of Sanitary Permit',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Health and Medical Certificate',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Working Permits',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Certificate Town Sites Sales Application',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Provision of Financial and Relief Assistance',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of New and Renewal of Business Permit',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of True Copy of Tax Declaration',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Certificate of Assessment',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Building Permits',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Certificate of Occupancy',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Securing Civil Registry Documents',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Registration of Legal Documents',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Petition for Correction of Clerical Error/Change of Name',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Application for Marriage',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Parks and Recreation Permit',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Sewer Compliance Certificate',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Tree Cutting Permit',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Issuance of Mayors Clearance',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Provision of Emergency and Shelter Assistance',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Pre-marriage Counseling',
		       	'description' => ''
		    )
		));
    }
}
